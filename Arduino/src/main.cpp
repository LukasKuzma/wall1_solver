#include "solver.h"
#include <Arduino.h>
#include <SoftwareSerial.h>

SoftwareSerial softSerial(1, 0);

int led = 13;
const size_t buffer_size = 80;
char buffer[buffer_size];
String current_equation;

void setup() {
  pinMode(led, OUTPUT);
  softSerial.begin(9600);
}

void loop() {
  memset(buffer, 0, sizeof(buffer));
  softSerial.readBytesUntil('\n', buffer, buffer_size);
  String input(buffer);
  input = input.substring(0, input.length() - 1);
  if (input.length() != 0){
    current_equation = input;
    softSerial.println(solve(current_equation));
  }
}