#include <stdlib.h>
#include <string.h>
#include "stm32l4xx_hal.h"
#include "functions.h"
#include "solver.h"

#define buffer_size 64
#define rx_buffer_size 256
uint8_t rx_buffer[rx_buffer_size];
uint8_t tx_buffer[buffer_size];
uint8_t current_equation[rx_buffer_size];
UART_HandleTypeDef UartHandle;
size_t rx_byte_counter = 0;
uint8_t rx_byte;

void SystemClock_Config(void);
void GPIO_Init();
void USART_DeInit();
void USART_Init();

int main(void){
    HAL_Init();
    GPIO_Init();
    USART_Init();

    while(1){
        HAL_UART_Receive_IT(&UartHandle, &rx_byte, 1);  
    }
}

void SysTick_Handler(void)
{
    HAL_IncTick();
}

void USART1_IRQHandler(void)
{
    HAL_UART_IRQHandler(&UartHandle);
}

void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{
    rx_buffer[rx_byte_counter] = rx_byte;
    rx_byte_counter++;
    if (rx_byte_counter == rx_buffer_size){
        rx_byte_counter = 0;
    }
    if (rx_byte == '\n'){
        memset(current_equation, '\0', buffer_size);
        memcpy(current_equation, rx_buffer, strlen((char*) rx_buffer));
        memset(tx_buffer, '\0', buffer_size);
        memset(rx_buffer, '\0', rx_buffer_size);
        int result = solve((char*) current_equation);
        itoa(result, (char*) tx_buffer, 10);
        tx_buffer[strlen((char*) tx_buffer)] = '\r';
        tx_buffer[strlen((char*) tx_buffer)] = '\n';
        HAL_UART_Transmit(&UartHandle, tx_buffer, strlen((char*) tx_buffer), 1000);
        rx_byte_counter = 0;
    }
}

void GPIO_Init(){
    __HAL_RCC_GPIOA_CLK_ENABLE();
    GPIO_InitTypeDef GPIO_InitStruct;
    GPIO_InitStruct.Pin = GPIO_PIN_9;
    GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
    GPIO_InitStruct.Pull = GPIO_PULLUP;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
    GPIO_InitStruct.Alternate = GPIO_AF7_USART1;
    HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

    GPIO_InitStruct.Pin = GPIO_PIN_10;
    HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);
}

void USART_Init(){
    __HAL_RCC_USART1_CLK_ENABLE();

    UartHandle.Instance = USART1;
    UartHandle.Init.BaudRate = 9600;
    UartHandle.Init.WordLength = UART_WORDLENGTH_8B;
    UartHandle.Init.StopBits = UART_STOPBITS_1;
    UartHandle.Init.Parity = UART_PARITY_NONE;
    UartHandle.Init.HwFlowCtl = UART_HWCONTROL_NONE;
    UartHandle.Init.Mode = UART_MODE_TX_RX;
    UartHandle.Init.OverSampling = UART_OVERSAMPLING_16;
    UartHandle.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
    HAL_UART_Init(&UartHandle);

    HAL_NVIC_SetPriority(USART1_IRQn, 0, 0);
    HAL_NVIC_EnableIRQ(USART1_IRQn);

    __HAL_UART_ENABLE_IT(&UartHandle, UART_IT_RXNE);
}

void USART_DeInit(){
    __HAL_RCC_USART1_FORCE_RESET();
    __HAL_RCC_USART1_RELEASE_RESET();

    HAL_GPIO_DeInit(GPIOA, GPIO_PIN_9);
    HAL_GPIO_DeInit(GPIOA, GPIO_PIN_10);
}
