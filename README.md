# wall1 Solver

Šioje repozitorijoje yra sprendžiamas wall1 programos uždavinys. Uždavinį pateikia Nano v3 mikrokontroleris, uždavinį sprendžia Nucleo 64 mikrokontroleris.

## Sujungimas

| Nano v3 | Nucleo 64 |
|---|---|
| 5V | 5V |
| GND | GND |
| D4 | D8 |
| D5 | D2 |

## Sprendimas

wall1 pateikia lygtį, kuriai reikia rasti x. Bendru atveju wall1 lygtį galime užrašyti taip: `a*x+b*(x^2)+x/c-d=0`. Lygtyje gali nebūti iki dviejų narių su `a`, `b` ir `c`. Nariai visada išdėstyti tokia tvarka, kaip pateiktoje lygtyje. `d` negali būti lygus 0, `a`, `b` ir `c` visada daugiau arba lygūs 0. `x` visada teigiamas sveikasis skaičius 

Lygčiai išspręsti pirmiausia randame `a`, `b`, `c` ir `d` reikšmes. Narių paieška vykdoma nuo lygties galo. 
1. Tikriname ar narys egzistuoja lygtyje. Narį c galime rasti jei lygtyje yra simbolis `/`, b jei egzistuoja `^`, a jei egzistuoja `*` (`*` egzistuoja ir `b` naryje, bet kai tikriname `a`, `b` jau bus tikrai ištrintas).
2. Jei egzistuoja surandame jį ir nuskaitome jo reikšmę. Jei ne priskiriame 0.
3. Nukertame išanalizuotą lygties dalį ir ieškome sekančio nario.

Kai randami visi nariai pašaliname `c` narį, jei jis yra. `a`, `b` ir `d` padauginame iš `c`, o prie `a` dar pridedame 1.

Tuomet priklausomai nuo to, kokie nariai nėra lygus 0, nustatome, kokią lygtį reikia spręsti:
1. Jei `b = 0`, sprendžiame lygtį `a*x-d=0`.
2. Jei `b != 0` ir `a = 0` sprendžiame lygtį  `b*(x^2)-d=0`
3. Jei `b != 0` ir `a != 0` sprendžiame lygtį `b*(x^2)+a*x-d=0`. Šiuo atveju išrenkame tik sveikąjį sprendinį.

Gautą sprendinį išsiunčiame į Nano v3.
