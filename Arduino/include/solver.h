#ifndef SOLVER_H
#define SOLVER_H

#include <Arduino.h>

int get_d(String &equation);
int get_c(String &equation);
int get_b(String &equation);
int get_a(String &equation);

int solve_quadratic(int a, int b, int c);
int solve_simple_quadratic(int a, int c);
int solve_linear(int a, int c);

int solve(String equation);

#endif