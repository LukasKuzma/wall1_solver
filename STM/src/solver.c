#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "solver.h"
#include "functions.h"

int get_d(char* equation){
    const int equals_index = find_index_end(equation, '=');
    const int minus_index = find_index_end(equation, '-');
    char buffer[equals_index - minus_index];
    strncpy(buffer, equation + minus_index, equals_index - minus_index);
    memset(equation + minus_index, '\0', strlen(equation) - minus_index);
    return atoi(buffer);
}

int get_c(char* equation){
    const int division_index = find_index_end(equation, '/');
    if (division_index != -1){
        const int length = strlen(equation);
        char buffer[length - division_index - 1];
        strncpy(buffer, equation + division_index + 1, length - division_index - 1);
        const int plus_index = find_index_end(equation, '+');
        memset(equation + plus_index, '\0', length - plus_index);
        return atoi(buffer);
    } else return 0;
}

int get_b(char* equation){
    const int square_index = find_index_end(equation, '^');
    if (square_index != -1){
        const int plus_index = find_index_end(equation, '+') + 1;
        const int mult_index = find_index_end(equation, '*');
        char buffer[mult_index - plus_index];
        strncpy(buffer, equation + plus_index, mult_index - plus_index);
        memset(equation + plus_index - 1, '\0', strlen(equation) - plus_index);
        return atoi(buffer);
    } else return 0;
}

int get_a(char* equation){
    const int mult_index = find_index_end(equation, '*');
    if (mult_index != -1){
        char buffer[mult_index];
        strncpy(buffer, equation, mult_index);
        return atoi(buffer);
    } else return 0;
}

int solve_quadratic(int a, int b, int c){
    double d = sqrt(b*b - 4*a*c);
    double r1 = (-b + d)*1.0/(2*a);
    double r2 = (-b - d)*1.0/(2*a);
    if (r1 == (int)r1 && r1 > 0)
        return (int)r1;
    else return (int) r2;
}

int solve_simple_quadratic(int a, int c){
    return (int)sqrt(-c/a);
}

int solve_linear(int a, int c){
    return -c/a;
}

// a*x+b*(x^2)+x/c-d=0

int solve(char* equation){
    char equation_copy[strlen(equation)];
    strcpy(equation_copy, equation);
    int a, b, c, d;
    d = get_d(equation_copy);
    c = get_c(equation_copy);
    b = get_b(equation_copy);
    a = get_a(equation_copy);
    if (c != 0){
        a *= c;
        b *= c;
        d *= c;
        a++;
    }

    if (b != 0){
        if (a != 0){
            return solve_quadratic(b, a, d);
        }
        else return solve_simple_quadratic(b, d);
    }
    else return solve_linear(a, d);
}

// x/4-1=0
// x=4

// x/18-1=0
// x=18

// 11*x-165=0
// x=15

// 14*x+4*(x^2)-120=0
// x=4

// 8*x+17*(x^2)+x/5-1782=0
// x=10

// 2*x+9*(x^2)-592=0
// x=8

// 3*x+x/1-12=0
// x=3

// 8*x+2*(x^2)+x/6-121=0
// x=6

// 15*(x^2)-540=0
// x=6

// 35 2A 28 78 5E 32 29 2D   36 30 35 3D 30 0D 0A