#include "functions.h"
#include <Arduino.h>
#include <SoftwareSerial.h>

int find_index_end(String string, char c){
    for (int i = string.length() - 1; i >= 0; i--){
        if (string.charAt(i) == c)
            return i;
    }
    return -1;
}