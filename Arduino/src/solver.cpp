#include "solver.h"
#include "functions.h"
#include <Arduino.h>
#include <SoftwareSerial.h>

int get_d(String &equation){
    equation = equation.substring(0, find_index_end(equation, '='));
    int index = find_index_end(equation, '-');
    String dStr = equation.substring(index, equation.length());
    equation = equation.substring(0, index);
    return dStr.toInt();
}

int get_c(String &equation){
    int divisionIndex = find_index_end(equation, '/');
    if (divisionIndex != -1){
        String cStr = equation.substring(divisionIndex + 1, equation.length());
        int index = find_index_end(equation, '+');
        equation = equation.substring(0, index);
        return cStr.toInt();
    } else return 0;
}

int get_b(String &equation){
    int squareIndex = find_index_end(equation, '^');
    if (squareIndex != -1){
        int index = find_index_end(equation, '+');
        String bStr = equation.substring(index + 1, equation.length());
        int squareMultIndex = find_index_end(bStr, '*');
        bStr = bStr.substring(0, squareMultIndex);
        if (index == -1)
            equation = "";
        else equation = equation.substring(0, index);
        return bStr.toInt();
    } else return 0;
}

int get_a(String &equation){
    int index = find_index_end(equation, '*');
    if (index != -1){
        String aStr = equation.substring(0, index);
        return aStr.toInt();
    } else return 0;
}

int solve_quadratic(int a, int b, int c){
    double d = sqrt(b*b - 4*a*c);
    double r1 = (-b + d)*1.0/(2*a);
    double r2 = (-b - d)*1.0/(2*a);
    if (r1 == (int)r1 && r1 > 0)
        return (int)r1;
    else return (int) r2;
}

int solve_simple_quadratic(int a, int c){
    return (int)sqrt(-c/a);
}

int solve_linear(int a, int c){
    return -c/a;
}

// a*x+b*(x^2)+x/c-d=0

int solve(String equation){
    int a, b, c, d;
    d = get_d(equation);
    c = get_c(equation);
    b = get_b(equation);
    a = get_a(equation);
    if (c != 0){
        a *= c;
        b *= c;
        d *= c;
        a++;
    }

    if (b != 0){
        if (a != 0){
            return solve_quadratic(b, a, d);
        }
        else return solve_simple_quadratic(b, d);
    }
    else return solve_linear(a, d);
}

// x/4-1=0
// x=4

// x/18-1=0
// x=18

// 11*x-165=0
// x=15

// 14*x+4*(x^2)-120=0
// x=4

// 8*x+17*(x^2)+x/5-1782=0
// x=10

// 2*x+9*(x^2)-592=0
// x=8

// 3*x+x/1-12=0
// x=3

// 8*x+2*(x^2)+x/6-121=0
// x=6

// 15*(x^2)-540=0
// x=6