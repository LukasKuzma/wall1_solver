#ifndef SOLVER_H
#define SOLVER_H

int get_d(char* equation);
int get_c(char* equation);
int get_b(char* equation);
int get_a(char* equation);

int solve_quadratic(int a, int b, int c);
int solve_simple_quadratic(int a, int c);
int solve_linear(int a, int c);

int solve(char* equation);

#endif