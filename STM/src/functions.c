#include "string.h"
#include "functions.h"

int find_index_end(char* str, char c){
    for (int i = strlen(str) - 1; i >= 0; i--){
        if (str[i] == c)
            return i;
    }
    return -1;
}
